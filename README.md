# Projet de Programmation Fontionnelle 2020/2021
## Implémentation d'un modèle de calcul : automates cellulaires

Réalisé par Romain Fortineau et Félix Saget, du groupe 685K de la L3 Informatique de l'Université de Nantes.

### Compilation

#### Dépendances

La compilation nécessite l'installation du module `OCaml Graphics` :

    opam install graphics

#### Création de l'exécutable

    git clone https://gitlab.univ-nantes.fr/E187076T/projet-pf-automates-cellulaires.git
    cd projet-pf-automates-cellulaires
    cd src
    make
    ./projet
