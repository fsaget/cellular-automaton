open Grid;;

let test_aut_list = (
  { currentState = Infected }::{ currentState = Sane }      ::{ currentState = Sane }    ::{ currentState = Infected }  ::{ currentState = Vaccinated }::
  { currentState = Sane }    ::{ currentState = Vaccinated }::{ currentState = Sane }    ::{ currentState = Vaccinated }::{ currentState = Sane }::
  { currentState = Sane }    ::{ currentState = Vaccinated }::{ currentState = Infected }::{ currentState = Sane }      ::{ currentState = Vaccinated }::
  { currentState = Sane }    ::{ currentState = Vaccinated }::{ currentState = Infected }::{ currentState = Sane }      ::{ currentState = Sane }::
  { currentState = Infected }::{ currentState = Sane }      ::{ currentState = Sane }    ::{ currentState = Infected }  ::{ currentState = Sane }::[]
);;

let test_loc_rule n a = 
  let count_states_in_nbh_list l value =
    List.length (List.filter (fun s -> s = value) l)
  in match a.currentState with
  | Infected -> begin match get_neighbors n a with
    | l_infvac when (count_states_in_nbh_list l_infvac { currentState = Vaccinated }) > 2 -> { currentState = Vaccinated }
    | _ -> { currentState = Dead } end
  | Sane -> begin match get_neighbors n a with
    | l_saninf when (count_states_in_nbh_list l_saninf { currentState = Infected }) > 0 -> { currentState = Infected }
    | l_sanvac when (count_states_in_nbh_list l_sanvac { currentState = Vaccinated }) > 0 -> { currentState = Vaccinated }
    | _ -> a end
  | _ -> a;;

let test_network = Network (test_aut_list, test_loc_rule);;

(* overall test *)
(match generate test_network with
| Network (l_result, _) -> (l_result =
  [ {currentState = Dead};        {currentState = Infected};
    {currentState = Infected};    {currentState = Dead};
    {currentState = Vaccinated};  {currentState = Infected};
    {currentState = Vaccinated};  {currentState = Infected};
    {currentState = Vaccinated};  {currentState = Infected};
    {currentState = Vaccinated};  {currentState = Vaccinated};
    {currentState = Vaccinated};  {currentState = Infected};
    {currentState = Vaccinated};  {currentState = Infected};
    {currentState = Vaccinated};  {currentState = Dead};
    {currentState = Infected};    {currentState = Infected};
    {currentState = Dead};        {currentState = Infected};
    {currentState = Infected};    {currentState = Dead};
    {currentState = Infected}]));;