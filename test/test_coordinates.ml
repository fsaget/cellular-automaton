open Coordinates;;

let test_grid = 
  'a'::'b'::'c'::'d'::'e'::
  'f'::'g'::'h'::'i'::'j'::
  'k'::'l'::'m'::'n'::'o'::
  'p'::'q'::'r'::'s'::'t'::
  'u'::'v'::'w'::'x'::'y'::[];;

(* get_index *)
(get_index test_grid 'i') = 8;;
(get_index test_grid 'l') = 11;;
(get_index test_grid 'y') = 24;;

(* get_x *)
(get_x test_grid 'i') = 1;;
(get_x test_grid 'l') = 2;;
(get_x test_grid 'y') = 4;;

(* get_y *)
(get_y test_grid 'i') = 3;;
(get_y test_grid 'l') = 1;;
(get_y test_grid 'y') = 4;;
