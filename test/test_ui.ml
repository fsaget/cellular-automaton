open Graphics;;
open Automaton;;
open Coordinates;;
open Ui;;

Graphics.open_graph " 500x500";;

draw (
  [ {currentState = Dead};      {currentState = Infected};
  {currentState = Infected};    {currentState = Dead};
  {currentState = Vaccinated};  {currentState = Infected};
  {currentState = Vaccinated};  {currentState = Infected};
  {currentState = Vaccinated};  {currentState = Infected};
  {currentState = Vaccinated};  {currentState = Vaccinated};
  {currentState = Vaccinated};  {currentState = Infected};
  {currentState = Vaccinated};  {currentState = Infected};
  {currentState = Vaccinated};  {currentState = Dead};
  {currentState = Infected};    {currentState = Infected};
  {currentState = Dead};        {currentState = Infected};
  {currentState = Infected};    {currentState = Dead};
  {currentState = Infected}]
);;

read_line ();;