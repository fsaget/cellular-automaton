(* Romain Fortineau & Félix Saget - groupe 685K *)

open Graphics;;
open Automaton;;
open Coordinates;;

(*  Réalise un magnifique dessin d'une liste d'automates *)
let draw l_aut =
  Graphics.clear_graph ();

  let matrix_side =
    int_of_float (sqrt (float_of_int (List.length l_aut)))
  in let draw_aut aut = begin match aut.currentState with
    | Sane       -> set_color (rgb 33 255 6)    (* green *) 
    | Infected   -> set_color (rgb 255 0 0)     (* red *) 
    | Vaccinated -> set_color (rgb 0 174 238)   (* blue *)
    | Dead       -> set_color (rgb 127 127 127) (* gray *) 
    end;

    (* Remplissage *)
    let aut_x = (10 + ((get_y l_aut aut) * (480 / matrix_side)))
    in let aut_y = (490 - (((get_x l_aut aut) + 1) * (480 / matrix_side))) 
    in Graphics.fill_rect aut_x aut_y    
    (480 / matrix_side) (480 / matrix_side)

    (*in print_string ("["^(string_of_int aut_x)^":"^(string_of_int aut_y)^"]")*)
  in List.map (draw_aut) (l_aut);;