(* Romain Fortineau & Félix Saget - groupe 685K *)

open Automaton;;
open Coordinates;;

(*  Le système d'automates cellulaires contient le réseau d'automates
    et la règle locale qui leur est commune.*)
type cellular_network = Network of automaton list * (cellular_network -> automaton -> automaton);;

(*  Retourne le voisinage de Moore d'un automate donné dans le réseau. *)
let get_neighbors n a =
  let is_neighbor l a1 a2 =
    a1 != a2
    && ((abs ((get_x l a1) - (get_x l a2))) < 2) 
    && ((abs ((get_y l a1) - (get_y l a2))) < 2)
  in match n with
  | Network (l_aut, _) -> List.filter (is_neighbor l_aut a) l_aut;;

(*  Applique la règle locale à tous les automates du réseau et 
    retourne un nouveau système contenant le nouveau réseau *)
let generate n = match n with
  | Network (l_aut, loc_rule) -> Network ((List.map (loc_rule n) l_aut), loc_rule);;