(* Romain Fortineau & Félix Saget - groupe 685K *)

open Automaton;;

(*  Retourne l'index de l'élément e dans la liste l. *)
let get_index l e =
  let rec index_aux ls el i = match ls with
    | [] -> raise(Not_found)
    | hd::tl -> if hd.uid == el.uid then i else index_aux tl el (i+1)
  in index_aux l e 0;;

(*  Retourne l'abscisse x de l'élément dans la liste l,
    comme si la liste était une matrice carrée. *)
let get_x l a =
  let a_index = get_index l a
  in let matrix_side = int_of_float (sqrt (float_of_int (List.length l)))
  in a_index / matrix_side;;

(*  Retourne l'ordonnée y de l'élément dans la liste l,
    comme si la liste était une matrice carrée. *)
let get_y l a =
  let a_index = get_index l a
  in let matrix_side = int_of_float (sqrt (float_of_int (List.length l)))
  in a_index mod matrix_side;;