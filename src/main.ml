(* Romain Fortineau & Félix Saget - L3 Informatique *)
(*      E185605U    &   E187076T  - groupe 685K*)
(* == Testé sur MacOS et Kubuntu == *)
(* Version OCaml: 4.10.0 & 4.05.0 *)

open Graphics;;
open Automaton;;
open Coordinates;;
open Grid;;
open Ui;;

(* === PARTIE INTERNE ===*)

(*  Configuration initiale de l'exemple *)
let example_aut_list = (
  { uid = 0; currentState = Infected } ::{ uid = 1; currentState = Sane }       ::{ uid = 2; currentState = Sane }     ::{ uid = 3; currentState = Infected }   ::{ uid = 4; currentState = Vaccinated }::
  { uid = 5; currentState = Sane }     ::{ uid = 6; currentState = Vaccinated } ::{ uid = 7; currentState = Sane }     ::{ uid = 8; currentState = Vaccinated } ::{ uid = 9; currentState = Sane }::
  { uid = 10; currentState = Sane }    ::{ uid = 11; currentState = Vaccinated }::{ uid = 12; currentState = Infected }::{ uid = 13; currentState = Sane }      ::{ uid = 14; currentState = Vaccinated }::
  { uid = 15; currentState = Sane }    ::{ uid = 16; currentState = Vaccinated }::{ uid = 17; currentState = Infected }::{ uid = 28; currentState = Sane }      ::{ uid = 19; currentState = Sane }::
  { uid = 20; currentState = Infected }::{ uid = 21; currentState = Sane }      ::{ uid = 22; currentState = Sane }    ::{ uid = 23; currentState = Infected }  ::{ uid = 24; currentState = Sane }::[]
);;

(*  Règle locale du système *)
let example_loc_rule n a = 
  let count_states_in_nbh_list l value =
    List.length (List.filter (fun s -> s.currentState = value) l)
  in match a.currentState with
  | Infected -> begin match get_neighbors n a with
    | l_infvac when (count_states_in_nbh_list l_infvac (Vaccinated)) > 2 -> { uid = a.uid; currentState = Vaccinated }
    | _ -> { uid = a.uid; currentState = Dead } end
  | Sane -> begin match get_neighbors n a with
    | l_saninf when (count_states_in_nbh_list l_saninf (Infected)) > 0 -> { uid = a.uid; currentState = Infected }
    | l_sanvac when (count_states_in_nbh_list l_sanvac (Vaccinated)) > 0 -> { uid = a.uid; currentState = Vaccinated }
    | _ -> a end
  | _ -> a;;

(*  Construction du système *)
let example_network = Network (example_aut_list, example_loc_rule);;

(* === PARTIE GRAPHIQUE === *)

Graphics.open_graph " 500x500";;

draw example_aut_list;;   (* Dessin de la configuration initiale *)

read_key ();;

(*  Boucle de génération du système *)
let rec run nwk =
  let next_generation =
    generate nwk
  in match next_generation with
  | Network (nw_list, _) -> draw nw_list; read_key (); run next_generation;;

run example_network;;