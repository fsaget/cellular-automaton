(* Romain Fortineau & Félix Saget - groupe 685K *)

(*  Le type state est un type somme incluant 
    les quatre états de notre exemple. *)
type state = Sane | Vaccinated | Infected | Dead;;

(*  Le type automaton englobe l'état de la cellule,
    parmi les quatre éléments du type state. *)
type automaton = { uid: int; currentState: state };;